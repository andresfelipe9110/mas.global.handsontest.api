using Mas.Global.HandsOnTest.Application.Employees;
using Mas.Global.HandsOnTest.Application.Rules.Factory;
using Mas.Global.HandsOnTest.Core.Employees;
using Xunit;

namespace Mas.Global.HandsOnTest.Tests
{
    public class EmployeeServiceTests
    {
        private IEmployeeService _employeeServices;
        private IEmployeeRepository _employeeRepository;
        private ICalculateSalaryFactory _calculateSalaryFactory;

        public EmployeeServiceTests()
        {
            _employeeRepository = new EmployeeRepository();
            _calculateSalaryFactory = new CalculateSalaryFactory();
        }

        [Fact]
        public void CalculateAnnualSalaryByHourly()
        {
            /// Employee service
            _employeeServices = new EmployeeService(_employeeRepository, _calculateSalaryFactory);

            /// Employee with annual salary
            var employeesCalculated = _employeeServices.GetEmployeeById(1);

            /// Salary verification
            Assert.Equal(86400000, employeesCalculated.AnnualSalary);
        }

        [Fact]
        public void CalculateAnnualSalaryByMonthly()
        {
            /// Employee service
            _employeeServices = new EmployeeService(_employeeRepository, _calculateSalaryFactory);

            /// Employee with annual salary
            var employeesCalculated = _employeeServices.GetEmployeeById(2);

            /// Salary verification
            Assert.Equal(960000, employeesCalculated.AnnualSalary);
        }
    }
}
