﻿using Mas.Global.HandsOnTest.Application.Employees.Dto;
using System.Collections.Generic;

namespace Mas.Global.HandsOnTest.Application.Result
{
    public class ResultEmployeesDto
    {
        public IEnumerable<EmployeeDto> EmployeesDto { get; set; }

        public string Message { get; set; }

        public string Result { get; set; }
    }
}
