﻿using Mas.Global.HandsOnTest.Application.Employees.Dto;

namespace Mas.Global.HandsOnTest.Application.Result
{
    public class ResultEmployeeDto
    {
        public EmployeeDto EmployeeDto { get; set; }

        public string Message { get; set; }

        public string Result { get; set; }
    }
}
