﻿using Mas.Global.HandsOnTest.Application.Employees.Dto;
using Mas.Global.HandsOnTest.Application.Rules.Factory;
using Mas.Global.HandsOnTest.Core.Employees;
using System.Collections.Generic;
using System.Linq;

namespace Mas.Global.HandsOnTest.Application.Employees
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ICalculateSalaryFactory _calculateSalaryFactory;

        public EmployeeService(
            IEmployeeRepository employeeRepository,
            ICalculateSalaryFactory calculateSalaryFactory
        )
        {
            _employeeRepository = employeeRepository;
            _calculateSalaryFactory = calculateSalaryFactory;
        }

        /// <summary>
        /// Method to get all employee with annual salary
        /// </summary>
        /// <returns> List of employees with annual salary </returns>
        public IEnumerable<EmployeeDto> GetEmployees()
        {
            var employees = _employeeRepository.GetData();
            return employees.Select(e => new EmployeeDto()
            {
                Id = e.Id,
                Name = e.Name,
                ContractTypeName = e.ContractTypeName,
                RoleId = e.RoleId,
                RoleName = e.RoleName,
                RoleDescription = e.RoleDescription,
                HourlySalary = e.HourlySalary,
                MonthlySalary = e.MonthlySalary,
                AnnualSalary = e != null ? _calculateSalaryFactory.GetAnnualSalary(e).CalculateAnnualSalary() : 0,
            });
        }

        /// <summary>
        /// Method to get an employee by id with annual salary
        /// </summary>
        /// <param name="id"> Employee id </param>
        /// <returns> Employee with annual salary </returns>
        public EmployeeDto GetEmployeeById(int id)
        {
            var employees =  _employeeRepository.GetData();
            var employee = employees.FirstOrDefault(e => e.Id == id);
            if (employee != null)
            {
                return new EmployeeDto()
                {
                    Id = employee.Id,
                    Name = employee.Name,
                    ContractTypeName = employee.ContractTypeName,
                    RoleId = employee.RoleId,
                    RoleName = employee.RoleName,
                    RoleDescription = employee.RoleDescription,
                    HourlySalary = employee.HourlySalary,
                    MonthlySalary = employee.MonthlySalary,
                    AnnualSalary = employee != null ?_calculateSalaryFactory.GetAnnualSalary(employee).CalculateAnnualSalary() : 0,
                };
            }
            return null;
        }
    }
}
