﻿using Mas.Global.HandsOnTest.Application.Employees.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mas.Global.HandsOnTest.Application.Employees
{
    public interface IEmployeeService
    {
        EmployeeDto GetEmployeeById(int id);

        IEnumerable<EmployeeDto> GetEmployees();

    }
}
