﻿using Mas.Global.HandsOnTest.Core.Employees;

namespace Mas.Global.HandsOnTest.Application.Rules.Factory
{
    public interface ICalculateSalaryFactory
    {
        CalculateSalary GetAnnualSalary(Employee employee);
    }
}
