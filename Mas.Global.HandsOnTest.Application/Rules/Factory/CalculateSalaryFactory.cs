﻿using Mas.Global.HandsOnTest.Core.Employees;

namespace Mas.Global.HandsOnTest.Application.Rules.Factory
{
    public class CalculateSalaryFactory : ICalculateSalaryFactory
    {
        /// <summary>
        /// Business logic to calculate salary according to the type name contract
        /// </summary>
        /// <param name="employee"> Employee who annual salary is calculated </param>
        /// <returns> Calculated annual salary </returns>
        public CalculateSalary GetAnnualSalary(Employee employee)
        {
            switch (employee.ContractTypeName)
            {
                case "HourlySalaryEmployee":
                    return new CalculateHourlySalary(employee);
                case "MonthlySalaryEmployee":
                    return new CalculateMonthtlySalary(employee);
                default:
                    return null;
            }
        }
    }
}
