﻿namespace Mas.Global.HandsOnTest.Application.Rules
{
    public abstract class CalculateSalary
    {
        public abstract decimal CalculateAnnualSalary();
    }
}
