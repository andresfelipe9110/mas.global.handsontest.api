﻿using Mas.Global.HandsOnTest.Core.Employees;

namespace Mas.Global.HandsOnTest.Application.Rules
{
    public class CalculateMonthtlySalary : CalculateSalary
    {
        public readonly Employee _employee;

        /// <summary>
        /// Calculate monthly salary constructor
        /// </summary>
        /// <param name="employee"> Employee who annual salary is calculated </param>
        public CalculateMonthtlySalary(Employee employee)
        {
            _employee = employee;
        }

        /// <summary>
        /// Business rule to calculate monthly salary
        /// </summary>
        /// <returns> Calculated annual salary </returns>
        public override decimal CalculateAnnualSalary()
        {
            return _employee.MonthlySalary * 12;
        }
    }
}
