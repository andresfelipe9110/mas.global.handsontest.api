﻿using Mas.Global.HandsOnTest.Core.Employees;

namespace Mas.Global.HandsOnTest.Application.Rules
{
    public class CalculateHourlySalary : CalculateSalary
    {
        public readonly Employee _employee;

        /// <summary>
        /// Calculate hourly salary constructor
        /// </summary>
        /// <param name="employee"> Employee who annual salary is calculated </param>
        public CalculateHourlySalary(Employee employee)
        {
            _employee = employee;
        }

        /// <summary>
        /// Business rule to calculate hourly salary
        /// </summary>
        /// <returns> Calculated annual salary </returns>
        public override decimal CalculateAnnualSalary()
        {
            return 120 * _employee.HourlySalary * 12;
        }
    }
}
