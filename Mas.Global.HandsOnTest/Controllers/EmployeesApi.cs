﻿using Mas.Global.HandsOnTest.Application.Employees;
using Mas.Global.HandsOnTest.Application.Result;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Mas.Global.HandsOnTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesApi : ControllerBase
    {

        private readonly IEmployeeService _employeeService;

        public EmployeesApi(
            IEmployeeService employeeService
        )
        {
            _employeeService = employeeService;
        }

        /// <summary>
        /// API to get all employees
        /// </summary>
        /// <returns> Result with employees and message </returns>
        [HttpGet("GetAllEmployees")]
        [EnableCors("AllowOrigin")]
        public ResultEmployeesDto GetAllEmployees()
        {
            ResultEmployeesDto result = new ResultEmployeesDto();
            try
            {
                result.EmployeesDto = _employeeService.GetEmployees();
                result.Result = "Success";
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Result = "Error";
                return result;
            }
        }

        /// <summary>
        /// API to get an employee by id
        /// </summary>
        /// <param name="id"> Employee id </param>
        /// <returns> Result with employee and message </returns>
        [HttpGet("GetEmployeeById")]
        [EnableCors("AllowOrigin")]
        public ResultEmployeeDto GetEmployeeById(int id)
        {
            ResultEmployeeDto result = new ResultEmployeeDto();
            try
            {
                result.EmployeeDto = _employeeService.GetEmployeeById(id);
                result.Result = "Success";
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Result = "Error";
                return result;
            }
        }

    }
}
