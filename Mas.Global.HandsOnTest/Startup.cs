﻿using Mas.Global.HandsOnTest.Application.Employees;
using Mas.Global.HandsOnTest.Application.Rules.Factory;
using Mas.Global.HandsOnTest.Core.Employees;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Mas.Global.HandsOnTest
{
    public class Startup
    {
        private const string _apiVersion = "v1";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Dependency injection
            services.AddSingleton<IEmployeeService, EmployeeService>();
            services.AddSingleton<IEmployeeRepository, EmployeeRepository>();
            services.AddSingleton<ICalculateSalaryFactory, CalculateSalaryFactory>();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Swagger configuration
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(_apiVersion, new OpenApiInfo
                {
                    Title = "Mas Global API",
                    Version = _apiVersion,
                    Description = "REST API  HandsOnTest",
                    Contact = new OpenApiContact() { Name = "Mas Global", Email = "admin@masglobal.com" }

                });

            });

            // Enable cors for all domains 
            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            // Use swagger
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{_apiVersion}/swagger.json",  $"Mas Global API {_apiVersion}");

            });

            // Use cors for all domains
            app.UseCors(options => options.AllowAnyOrigin());
        }
    }
}
