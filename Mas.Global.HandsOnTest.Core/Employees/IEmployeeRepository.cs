﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mas.Global.HandsOnTest.Core.Employees
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetData();
    }
}
