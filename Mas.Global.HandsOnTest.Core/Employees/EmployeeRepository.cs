﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace Mas.Global.HandsOnTest.Core.Employees
{
    public class EmployeeRepository : IEmployeeRepository
    {
        /// <summary>
        /// Api url to consult employees 
        /// </summary>
        private const string URL = "http://masglobaltestapi.azurewebsites.net/api/Employees";

        /// <summary>
        /// Method to get the api employees
        /// </summary>
        /// <returns> List of employees </returns>
        public IEnumerable<Employee> GetData()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(URL).Result;

            var Items = new List<Employee>();

            IEnumerable<Employee> employees = null;
            if (response.IsSuccessStatusCode)
            {

                if (response.IsSuccessStatusCode)
                {
                    string apiResponse = response.Content.ReadAsStringAsync().Result;
                    employees = JsonConvert.DeserializeObject<IEnumerable<Employee>>(apiResponse);
                }
            }
             return employees;
        }
    }
}
